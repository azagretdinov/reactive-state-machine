package org.bitbucket.azagretdinov.reactivestatemachine;


import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

@Profile("client")
public interface ClientChannels {

    String ORDER_EVENTS_VIEW = "order-events-view";
    String ORDER_EVENTS = "order-events";

    @Output("order-command")
    MessageChannel orderCommand();

    @Input(ORDER_EVENTS)
    SubscribableChannel orderEvents();

    @Input(ORDER_EVENTS_VIEW)
    SubscribableChannel orderEventsView();

}

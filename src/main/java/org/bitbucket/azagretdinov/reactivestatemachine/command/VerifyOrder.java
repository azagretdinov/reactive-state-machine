package org.bitbucket.azagretdinov.reactivestatemachine.command;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.bitbucket.azagretdinov.reactivestatemachine.domain.Side;

import java.math.BigDecimal;
import java.util.Currency;

@Getter
public class VerifyOrder extends  AbstractDomainCommand {
    private final Side side;
    private final String symbol;
    private final Currency currency;
    private final BigDecimal quantity;

    @JsonCreator
    public VerifyOrder(
            @JsonProperty("id") String id,
            @JsonProperty("side") Side side,
            @JsonProperty("symbol") String symbol,
            @JsonProperty("currency") Currency currency,
            @JsonProperty("quantity") BigDecimal quantity
    ) {
        super(id, "verify-order");
        this.side = side;
        this.symbol = symbol;
        this.currency = currency;
        this.quantity = quantity;
    }
}
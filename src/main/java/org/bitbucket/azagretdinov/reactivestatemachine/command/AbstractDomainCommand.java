package org.bitbucket.azagretdinov.reactivestatemachine.command;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class AbstractDomainCommand implements DomainCommand {

    private final String type;
    private final String id;

    @JsonCreator
    public AbstractDomainCommand(
            @JsonProperty("id") final String id,
            @JsonProperty("type") final String type
    ) {
        this.id = id;
        this.type = type;
    }
}

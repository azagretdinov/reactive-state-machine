package org.bitbucket.azagretdinov.reactivestatemachine.command;

public interface DomainCommand {
    String getType();
    String getId();
}

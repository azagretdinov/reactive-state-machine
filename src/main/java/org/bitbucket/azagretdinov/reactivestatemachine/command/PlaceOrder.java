package org.bitbucket.azagretdinov.reactivestatemachine.command;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import org.bitbucket.azagretdinov.reactivestatemachine.model.OrderInstruction;

import java.time.LocalDate;
import java.time.LocalTime;

@ToString
@Getter
public class PlaceOrder extends AbstractDomainCommand {
    private final String orderId;
    private final OrderInstruction orderInstruction;
    private final LocalDate issueDate;
    private final LocalTime issueTime;
    private final String placedBy;

    @JsonCreator
    public PlaceOrder(
            @JsonProperty("orderId") String orderId,
            @JsonProperty("orderInstruction") final OrderInstruction orderInstruction,
            @JsonProperty("issueDate") final LocalDate issueDate,
            @JsonProperty("issueTime") final LocalTime issueTime,
            @JsonProperty("placedBy") final String placedBy) {
        super(orderId, "place-order");
        this.orderId = orderId;
        this.orderInstruction = orderInstruction;
        this.issueDate = issueDate;
        this.issueTime = issueTime;
        this.placedBy = placedBy;
    }
}

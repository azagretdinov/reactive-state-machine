package org.bitbucket.azagretdinov.reactivestatemachine;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.azagretdinov.reactivestatemachine.model.OrderInstruction;
import org.bitbucket.azagretdinov.reactivestatemachine.view.OrderView;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Profile("client")
@Controller
public class RestController {

    private final ClientOrderService clientOrderService;

    public RestController(ClientOrderService clientOrderService) {
        this.clientOrderService = clientOrderService;
    }

    @PutMapping(value = "/order/{orderId}", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    @ResponseBody
    public Flux<OrderEvent> placeOrder(
            @PathVariable final String orderId,
            @RequestBody final OrderInstruction orderInstruction
    ) {
        return clientOrderService.placeOrder(orderId, orderInstruction);
    }

    @GetMapping(value = "/order/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Mono<OrderView> getOrder(@PathVariable final String orderId){
        return clientOrderService.getOrder(orderId);
    }
}

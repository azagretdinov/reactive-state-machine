package org.bitbucket.azagretdinov.reactivestatemachine;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.azagretdinov.reactivestatemachine.command.PlaceOrder;
import org.bitbucket.azagretdinov.reactivestatemachine.event.DomainEvent;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderCompleted;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderPlaced;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderRejected;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderVerified;
import org.bitbucket.azagretdinov.reactivestatemachine.model.OrderInstruction;
import org.bitbucket.azagretdinov.reactivestatemachine.repository.OrderViewRepository;
import org.bitbucket.azagretdinov.reactivestatemachine.view.OrderView;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.EmitterProcessor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

@Slf4j
@Component
@Profile("client")
public class ClientOrderService {

    private final ObjectMapper objectMapper;
    private final MessageChannel orderCommand;
    private final OrderViewRepository orderViewRepository;
    private final Flux<OrderEvent> eventStream;
    private final Scheduler scheduler;
    private final FluxSink<OrderEvent> sink;

    public ClientOrderService(
            final ObjectMapper objectMapper,
            final @Qualifier("order-command") MessageChannel orderCommand,
            final OrderViewRepository orderViewRepository
    ) {
        this.objectMapper = objectMapper;
        this.orderCommand = orderCommand;
        this.orderViewRepository = orderViewRepository;

        var events = EmitterProcessor.<OrderEvent>create();
        eventStream = events.share();
        scheduler = Schedulers.newElastic("client-order-service");
        sink = events.sink(FluxSink.OverflowStrategy.LATEST);
    }

    public Flux<OrderEvent> placeOrder(final String orderId, final OrderInstruction orderInstruction){
        return Flux.<OrderEvent>create(sink -> placeOrder(orderId, orderInstruction, sink)).publishOn(scheduler);
    }

    private void placeOrder(String orderId, OrderInstruction orderInstruction, FluxSink<OrderEvent> sink) {
        eventStream
                .filter(e -> Objects.equals(orderId, e.getOrderId()))
                .subscribeOn(scheduler)
                .subscribe(new BaseSubscriber<>() {
                    @Override
                    protected void hookOnNext(OrderEvent value) {
                        log.info("publishing event: {}", value);
                        sink.next(value);
                        final var event = value.getDomainEvent();
                        if(event instanceof OrderCompleted){
                            log.info("Receive order completed event.");
                            sink.complete();
                        }else if(event instanceof OrderRejected){
                            log.info("Receive order rejected event.");
                            sink.complete();
                        }
                    }

                    @Override
                    protected void hookOnComplete() {
                        sink.complete();
                    }

                    @Override
                    protected void hookOnCancel() {
                        sink.complete();
                    }
                });

        final var command = new PlaceOrder(orderId, orderInstruction, LocalDate.now(), LocalTime.now(), null);

        log.info("Sending command {}", command);

        orderCommand.send(
                MessageBuilder.withPayload(command)
                        .setHeader("orderId", orderId)
                        .setHeader("type", command.getType())
                        .build()
        );
    }

    @StreamListener(target = ClientChannels.ORDER_EVENTS)
    public void listener(
            @Header("orderId") String orderId,
            @Header("type") String type,
            @Payload byte[] event
    ){
        final var orderEvent = new OrderEvent(orderId, deserialize(type, event));
        log.info("received a new event from server and publish internally: {}", orderEvent);
        sink.next(orderEvent);
    }

    private DomainEvent deserialize(String type, byte[] event) {
        try {
            switch (type){
                case "order-placed": return objectMapper.readValue(event, OrderPlaced.class);
                case "order-verified": return objectMapper.readValue(event, OrderVerified.class);
                case "order-completed": return objectMapper.readValue(event, OrderCompleted.class);
                case "order-rejected": return objectMapper.readValue(event, OrderRejected.class);
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public Mono<OrderView> getOrder(String orderId) {
        return Mono.fromCallable(() -> orderViewRepository.getOrderOrCreate(orderId));
    }
}

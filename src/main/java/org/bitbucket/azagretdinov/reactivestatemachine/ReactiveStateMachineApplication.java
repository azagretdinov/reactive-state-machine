package org.bitbucket.azagretdinov.reactivestatemachine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Profile;


@SpringBootApplication
public class ReactiveStateMachineApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactiveStateMachineApplication.class, args);
	}

	@Profile("client")
	@EnableBinding({
			ClientChannels.class
	})
	public static class ClientConfig{

	}


	@Profile("server")
	@EnableBinding({
			MiddlewareChannels.class
	})
	public static class ServerConfig{

	}
}

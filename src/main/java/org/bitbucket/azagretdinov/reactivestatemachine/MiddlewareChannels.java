package org.bitbucket.azagretdinov.reactivestatemachine;


import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

@Profile("server")
public interface MiddlewareChannels {

    String ORDER_EVENTS = "order-events";
    String ORDER_COMMAND = "order-command";

    @Input(ORDER_COMMAND)
    SubscribableChannel orderCommand();

    @Output(ORDER_EVENTS)
    MessageChannel orderEvents();

}

package org.bitbucket.azagretdinov.reactivestatemachine.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Currency;

@Getter
@ToString
public class OrderInstruction {


    private final String symbol;
    private final BigDecimal quantity;
    private final Currency currency;
    private final boolean sell;

    @JsonCreator
    public OrderInstruction(
            final @JsonProperty("symbol") String symbol,
            final @JsonProperty("quantity") BigDecimal quantity,
            final @JsonProperty("currency") Currency currency,
            final @JsonProperty("sell") boolean sell
    ) {
        this.symbol = symbol;
        this.quantity = quantity;
        this.currency = currency;
        this.sell = sell;
    }
}

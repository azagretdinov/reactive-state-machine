package org.bitbucket.azagretdinov.reactivestatemachine.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class AbstractDomainEvent implements DomainEvent {

    private final String id;
    private final String type;

    @JsonCreator
    public AbstractDomainEvent(
            @JsonProperty("id") final String id,
            @JsonProperty("type") final String type
    ) {
        this.id = id;
        this.type = type;
    }
}

package org.bitbucket.azagretdinov.reactivestatemachine.event;

public interface DomainEvent {

    String getType();

    String getId();

}

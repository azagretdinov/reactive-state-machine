package org.bitbucket.azagretdinov.reactivestatemachine.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import org.bitbucket.azagretdinov.reactivestatemachine.domain.Side;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Currency;

@ToString(callSuper = true)
@Getter
public class OrderPlaced extends AbstractDomainEvent {

    private final String symbol;
    private final BigDecimal quantity;
    private final Currency currency;
    private final LocalDate placeDate;
    private final LocalTime placeTime;
    private final LocalDate issueDate;
    private final LocalTime issueTime;
    private final String placedBy;
    private final Side side;

    @JsonCreator
    public OrderPlaced(
            @JsonProperty("id") final String id,
            @JsonProperty("symbol") final String symbol,
            @JsonProperty("quantity") final BigDecimal quantity,
            @JsonProperty("currency") final Currency currency,
            @JsonProperty("placeDate") final LocalDate placeDate,
            @JsonProperty("placeTime") final LocalTime placeTime,
            @JsonProperty("issueDate") final LocalDate issueDate,
            @JsonProperty("issueTime") final LocalTime issueTime,
            @JsonProperty("placedBy") final String placedBy,
            @JsonProperty("side") final Side side
    ) {
        super(id, "order-placed");
        this.symbol = symbol;
        this.quantity = quantity;
        this.currency = currency;
        this.placeDate = placeDate;
        this.placeTime = placeTime;
        this.issueDate = issueDate;
        this.issueTime = issueTime;
        this.placedBy = placedBy;
        this.side = side;
    }
}

package org.bitbucket.azagretdinov.reactivestatemachine.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper = true)
public class OrderRejected extends AbstractDomainEvent {
    private final String reason;

    @JsonCreator
    public OrderRejected(
            @JsonProperty("id") final String id,
            @JsonProperty("reason") final String reason
    ) {
        super(id, "order-rejected");
        this.reason = reason;
    }
}

package org.bitbucket.azagretdinov.reactivestatemachine.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import org.springframework.lang.Nullable;

import java.time.LocalTime;

@ToString(callSuper = true)
@Getter
public class OrderVerified extends AbstractDomainEvent {

    private final LocalTime verificationTime;

    private final String approvedBy;

    @JsonCreator
    public OrderVerified(
            @JsonProperty("id") final String id,
            @JsonProperty("verificationTime") final LocalTime verificationTime,
            @Nullable @JsonProperty("approvedBy") final String approvedBy) {
        super(id, "order-verified");
        this.verificationTime = verificationTime;
        this.approvedBy = approvedBy;
    }
}

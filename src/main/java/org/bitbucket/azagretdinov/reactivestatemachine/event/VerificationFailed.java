package org.bitbucket.azagretdinov.reactivestatemachine.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VerificationFailed extends AbstractDomainEvent {
    @JsonCreator
    public VerificationFailed(
            @JsonProperty("id") final String id,
            @JsonProperty("type") final String type
    ) {
        super(id, type);
    }
}

package org.bitbucket.azagretdinov.reactivestatemachine.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

@ToString(callSuper = true)
public class OrderCompleted extends AbstractDomainEvent {
    @JsonCreator
    public OrderCompleted(
            @JsonProperty("id") final String id
    ) {
        super(id, "order-completed");
    }
}

package org.bitbucket.azagretdinov.reactivestatemachine.domain;

import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Currency;

@Getter
@ToString
class OrderData {

    private final String orderId;

    private String symbol;
    private BigDecimal quantity;
    private Currency currency;
    private Side side;

    OrderData(String orderId) {
        this.orderId = orderId;
    }

    void addOrderInstruction(Side side, String symbol, BigDecimal quantity, Currency currency) {
        this.side = side;
        this.symbol = symbol;
        this.quantity = quantity;
        this.currency = currency;
    }
}

package org.bitbucket.azagretdinov.reactivestatemachine.domain;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.azagretdinov.reactivestatemachine.command.PlaceOrder;
import org.bitbucket.azagretdinov.reactivestatemachine.command.VerifyOrder;
import org.bitbucket.azagretdinov.reactivestatemachine.event.DomainEvent;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderCompleted;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderPlaced;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderVerified;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.StateMachineEventResult;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

import static org.bitbucket.azagretdinov.reactivestatemachine.domain.OrderFactory.EVENT;

@Slf4j
public class Order {

    private final Account account;
    private final OrderData orderData;
    private final StateMachine<State, Class<? extends DomainEvent>> orderStateMachine;
    private OrderSideFactory orderSideFactory;

    Order(
            final Account account, final OrderData orderData,
            final StateMachine<State, Class<? extends DomainEvent>> orderStateMachine,
            final OrderSideFactory orderSideFactory) {
        this.account = account;
        this.orderData = orderData;
        this.orderStateMachine = orderStateMachine;
        this.orderSideFactory = orderSideFactory;
    }

    public State getState() {
        final var state = orderStateMachine.getState();
        if (state != null) {
            return state.getId();
        }
        return null;
    }

    public Flux<Tuple2<State, DomainEvent>> process(Mono<PlaceOrder> placeOrder) {
        final var event = placeOrder
                .map(this::toEvent)
                .cast(DomainEvent.class);

        return apply(event)
                .flatMap(this::generalProcess)
                .log("order-" + orderData.getOrderId());
    }

    public Flux<Tuple2<State, DomainEvent>> processVerify(Mono<VerifyOrder> verifyOrder) {
        final var event = account.verify(verifyOrder);
        return apply(event)
                .flatMap(this::generalProcess)
                .log("order-" + orderData.getOrderId());
    }

    private Flux<Tuple2<State, DomainEvent>> generalProcess(Tuple2<State, DomainEvent> tuple) {
        final var event = tuple.getT2();
        log.info("processing event: {}", event);
        if (event instanceof OrderPlaced) {
            final var command = new VerifyOrder(
                    generateEventId(),
                    orderData.getSide(),
                    orderData.getSymbol(),
                    orderData.getCurrency(),
                    orderData.getQuantity()
            );
            return Flux.concat(
                    Mono.just(tuple),
                    processVerify(Mono.just(command))
            );
        }else if (event instanceof OrderVerified) {
            return Flux.concat(
                    Mono.just(tuple),
                    apply(Mono.just(new OrderCompleted(generateEventId())))
            );
        }
        return Flux.just(tuple);
    }

    private String generateEventId() {
        return UUID.randomUUID().toString();
    }

    private Flux<Tuple2<State, DomainEvent>> apply(Mono<DomainEvent> event) {
        log.info("sending event to state machine. state: {}", getState());
        return orderStateMachine.sendEvent(
                event.log("order-1")
                        .map(this::toMessage)
        ).log("order-" + orderData.getOrderId())
                .filter(result -> result.getResultType() != StateMachineEventResult.ResultType.DENIED)
                .map(result -> Tuples.of(
                        result.getRegion().getState().getId(),
                        result.getMessage()
                ))
                .map(t -> Tuples.of(
                        t.getT1(),
                        (DomainEvent) t.getT2().getHeaders().get(EVENT)
                        )
                );
    }

    private <T extends DomainEvent> Message<Class<? extends DomainEvent>> toMessage(T domainEvent) {
        final var aClass = domainEvent.getClass();
        return MessageBuilder.<Class<? extends DomainEvent>>withPayload(aClass)
                .setHeader(EVENT, domainEvent)
                .build();
    }

    private OrderPlaced toEvent(PlaceOrder event) {
        final var orderInstruction = event.getOrderInstruction();
        final var side = orderSideFactory.createSide(orderInstruction.isSell());
        return new OrderPlaced(
                event.getId(),
                orderInstruction.getSymbol(),
                orderInstruction.getQuantity(),
                orderInstruction.getCurrency(),
                LocalDate.now(),
                LocalTime.now(),
                event.getIssueDate(),
                event.getIssueTime(),
                event.getPlacedBy(),
                side
        );
    }

    public enum State {
        PendingNew,
        New,
        Verified,
        Invalid,
        Submitted,
        PendingCancel,
        PartiallyFilled,
        Replaced,
        Rejected,
        Cancelled,
        Filled
    }
}

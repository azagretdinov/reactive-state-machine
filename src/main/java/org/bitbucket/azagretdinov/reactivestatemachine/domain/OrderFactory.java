package org.bitbucket.azagretdinov.reactivestatemachine.domain;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.azagretdinov.reactivestatemachine.domain.Order.State;
import org.bitbucket.azagretdinov.reactivestatemachine.event.DomainEvent;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderCompleted;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderPlaced;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderSubmitted;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderVerified;
import org.bitbucket.azagretdinov.reactivestatemachine.event.VerificationFailed;
import org.springframework.context.annotation.Profile;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineBuilder;
import org.springframework.stereotype.Component;

@Profile("server")
@Component
@Slf4j
public class OrderFactory {

    public static final String EVENT = "event";

    private final OrderSideFactory orderSideFactory;

    public OrderFactory(OrderSideFactory orderSideFactory) {
        this.orderSideFactory = orderSideFactory;
    }

    public Order createOrder(final String orderId) throws Exception {
        final OrderData orderData = new OrderData(orderId);
        final StateMachine<State, Class<? extends DomainEvent>> orderStateMachine = buildStateMachine(orderData);
        return new Order(new Account(), orderData, orderStateMachine, orderSideFactory);
    }

    private StateMachine<State, Class<? extends DomainEvent>> buildStateMachine(OrderData orderData) throws Exception {
        final StateMachineBuilder.Builder<State, Class<? extends DomainEvent>> builder = StateMachineBuilder.builder();

        builder.configureConfiguration()
                .withConfiguration()
                    .autoStartup(true);

        builder.configureStates()
                .withStates()
                .initial(State.PendingNew)
                .state(State.New)
                .state(State.Verified)
                .state(State.Invalid)
                .state(State.Submitted)
                .state(State.PendingCancel)
                .state(State.PartiallyFilled)
                .state(State.Replaced)
                .end(State.Rejected)
                .end(State.Cancelled)
                .end(State.Filled);

        // @formatter:off

        builder.configureTransitions()
                .withExternal()
                    .source(State.PendingNew)
                    .target(State.New)
                    .event(OrderPlaced.class)
                    .action(context -> {
                        final OrderPlaced event = (OrderPlaced) context.getMessageHeader(EVENT);
                        orderData.addOrderInstruction(
                                event.getSide(),
                                event.getSymbol(),
                                event.getQuantity(),
                                event.getCurrency()
                        );
                    })
                .and()
                .withExternal()
                    .source(State.New)
                    .target(State.Verified)
                    .event(OrderVerified.class)
                .and()
                .withExternal()
                    .source(State.New)
                    .target(State.Invalid)
                    .event(VerificationFailed.class)
                .and()
                .withExternal()
                    .source(State.Verified)
                    .target(State.Submitted)
                    .event(OrderSubmitted.class)
                .and()
                .withExternal()
                    .source(State.Verified)
                    .target(State.Filled)
                    .event(OrderCompleted.class);

        // formatter:on

        return builder.build();
    }
}

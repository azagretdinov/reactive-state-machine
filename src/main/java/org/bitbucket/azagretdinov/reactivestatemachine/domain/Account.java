package org.bitbucket.azagretdinov.reactivestatemachine.domain;

import org.bitbucket.azagretdinov.reactivestatemachine.command.VerifyOrder;
import org.bitbucket.azagretdinov.reactivestatemachine.event.DomainEvent;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderVerified;
import reactor.core.publisher.Mono;

import java.time.LocalTime;
import java.util.UUID;

public class Account {
    public Mono<DomainEvent> verify(Mono<VerifyOrder> cmd) {
        return cmd
                .map(this::verify)
                .flatMap(Mono::just);
    }

    public DomainEvent verify(VerifyOrder cmd) {
        return new OrderVerified(
                UUID.randomUUID().toString(),
                LocalTime.now(),
                ""
        );
    }
}

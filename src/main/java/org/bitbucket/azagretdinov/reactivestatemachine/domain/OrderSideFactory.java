package org.bitbucket.azagretdinov.reactivestatemachine.domain;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("server")
@Component
public class OrderSideFactory {
    public Side createSide(boolean sell) {
        return sell ? Side.Sell : Side.Buy;
    }
}

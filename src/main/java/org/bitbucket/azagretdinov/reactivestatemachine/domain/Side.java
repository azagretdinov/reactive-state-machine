package org.bitbucket.azagretdinov.reactivestatemachine.domain;

public enum Side {
    Sell,
    Buy
}

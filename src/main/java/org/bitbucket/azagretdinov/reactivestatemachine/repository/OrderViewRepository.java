package org.bitbucket.azagretdinov.reactivestatemachine.repository;

import org.bitbucket.azagretdinov.reactivestatemachine.view.OrderView;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Repository
public class OrderViewRepository {
    private final ConcurrentMap<String, OrderView> data;

    public OrderViewRepository() {
        data = new ConcurrentHashMap<>();
    }

    public void save(OrderView orderView) {
        data.put(orderView.getOrderId(), orderView);
    }

    public OrderView getOrderOrCreate(final String orderId){
        return data.computeIfAbsent(orderId, this::createOrder);
    }

    private OrderView createOrder(String orderId) {
        final var orderView = new OrderView();
        orderView.setOrderId(orderId);
        return orderView;
    }
}

package org.bitbucket.azagretdinov.reactivestatemachine.repository;

import org.bitbucket.azagretdinov.reactivestatemachine.event.DomainEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Repository
@Profile("server")
public class OrderEventRepository {

    private final ConcurrentMap<String, List<DomainEvent>> data;

    public OrderEventRepository() {
        data = new ConcurrentHashMap<>();
    }

    public <T extends DomainEvent> Mono<T> save(String orderId, T domainEvent) {
        data.computeIfAbsent(orderId, k -> new CopyOnWriteArrayList<>()).add(domainEvent);
        return Mono.just(domainEvent);
    }

    public boolean hasOrderData(String orderId) {
        return data.containsKey(orderId);
    }
}

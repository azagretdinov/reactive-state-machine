package org.bitbucket.azagretdinov.reactivestatemachine.view;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;


@Data
public class OrderView {
    private String orderId;
    private String state;
    private String symbol;
    private BigDecimal quantity;
    private String currency;
    private String side;
    private LocalDate placeDate;
    private LocalTime placeTime;
    private LocalDate issueDate;
    private LocalTime issueTime;
    private String placedBy;
    private int currencyFractionDigits;
    private LocalTime verificationTime;
    private String approvedBy;
}

package org.bitbucket.azagretdinov.reactivestatemachine;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.bitbucket.azagretdinov.reactivestatemachine.event.DomainEvent;

@AllArgsConstructor
@ToString
@Getter
public class OrderEvent {
    private final String orderId;
    private final DomainEvent domainEvent;
}

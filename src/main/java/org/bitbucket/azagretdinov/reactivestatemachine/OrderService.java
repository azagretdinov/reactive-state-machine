package org.bitbucket.azagretdinov.reactivestatemachine;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.azagretdinov.reactivestatemachine.command.DomainCommand;
import org.bitbucket.azagretdinov.reactivestatemachine.command.PlaceOrder;
import org.bitbucket.azagretdinov.reactivestatemachine.domain.Order;
import org.bitbucket.azagretdinov.reactivestatemachine.domain.OrderFactory;
import org.bitbucket.azagretdinov.reactivestatemachine.event.DomainEvent;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderRejected;
import org.bitbucket.azagretdinov.reactivestatemachine.repository.OrderEventRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.EmitterProcessor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.core.publisher.UnicastProcessor;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuple3;
import reactor.util.function.Tuples;

import java.util.UUID;

@Slf4j
@Service
@Profile("server")
public class OrderService {
    private final OrderFactory orderFactory;
    private final OrderEventRepository orderEventRepository;

    private final Flux<Tuple3<String, Order.State, DomainEvent>> eventStream;
    private final Scheduler orderScheduler;
    private final Scheduler eventsScheduler;
    private final FluxSink<Tuple3<String, Order.State, DomainEvent>> eventsSink;

    public OrderService(OrderFactory orderFactory, OrderEventRepository orderEventRepository) {
        this.orderFactory = orderFactory;
        this.orderEventRepository = orderEventRepository;

        final UnicastProcessor<Tuple3<String, Order.State, DomainEvent>> events = UnicastProcessor.create();
        eventsSink = events.sink();
        eventStream = events.share();

        orderScheduler = Schedulers.newSingle("orders");
        eventsScheduler = Schedulers.newElastic("events");
    }

    public Flux<Tuple3<String, Order.State, DomainEvent>> getEventStream() {
        return eventStream;
    }

    public Mono<Void> placeOrder(String orderId, PlaceOrder placeOrder) {
        if(orderEventRepository.hasOrderData(orderId)){
            return reject(orderId, "Order already existed");
        }

        final Flux<Tuple3<String, Order.State, DomainEvent>> orderEvents;
        try {
            orderEvents = doPlaceOrder(orderId, placeOrder);
        } catch (Exception e) {
            log.error("Cannot place order {}", orderId, e);
            return reject(orderId, e.getMessage());
        }

        orderEvents
                .subscribeOn(eventsScheduler)
                .subscribe(new EventStreamSubscriber());

        return orderEvents
                .subscribeOn(eventsScheduler)
                .then(Mono.empty());
    }

    private Flux<Tuple3<String, Order.State, DomainEvent>> doPlaceOrder(
            final String orderId,
            final PlaceOrder placeOrder
    ) throws Exception {
        var order = orderFactory.createOrder(orderId);
        return order.process(wrapCommand(placeOrder))
                .subscribeOn(eventsScheduler)
                .flatMap(t ->
                        store(orderId, t.getT2())
                            .map(e -> Tuples.of(orderId, t.getT1(), t.getT2()))
                )
                .share();
    }

    private Mono<Void> reject(String orderId, String reason) { ;
        return Mono.fromRunnable(() -> eventsSink.next(
                Tuples.of(
                        orderId,
                        Order.State.Rejected,
                        new OrderRejected(
                                UUID.randomUUID().toString(),
                                reason
                        )
                )
        ));
    }

    private <T extends DomainEvent> Mono<T> store(final String orderId, final T domainEvent) {
        return orderEventRepository.save(orderId, domainEvent);
    }

    private <T extends DomainCommand> Mono<T> wrapCommand(T domainCommand) {
        return Mono.just(domainCommand).publishOn(orderScheduler);
    }

    private class EventStreamSubscriber extends BaseSubscriber<Tuple3<String, Order.State, DomainEvent>> {
        @Override
        protected void hookOnNext(Tuple3<String, Order.State, DomainEvent> value) {
            eventsSink.next(value);
        }

        @Override
        protected void hookOnError(Throwable throwable) {
            eventsSink.error(throwable);
        }
    }
}

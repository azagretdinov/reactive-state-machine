package org.bitbucket.azagretdinov.reactivestatemachine.messaing;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.azagretdinov.reactivestatemachine.MiddlewareChannels;
import org.bitbucket.azagretdinov.reactivestatemachine.OrderService;
import org.bitbucket.azagretdinov.reactivestatemachine.command.PlaceOrder;
import org.bitbucket.azagretdinov.reactivestatemachine.domain.Order;
import org.bitbucket.azagretdinov.reactivestatemachine.event.DomainEvent;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.reactive.StreamEmitter;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Slf4j
@Profile("server")
@Component
public class OrderMessaging {

    private final OrderService orderService;

    public OrderMessaging(OrderService orderService) {
        this.orderService = orderService;
    }

    @StreamListener(target = MiddlewareChannels.ORDER_COMMAND, condition = "headers['type']=='place-order'")
    public void placeOrder(
            @Header(KafkaHeaders.ACKNOWLEDGMENT) final Acknowledgment acknowledgment,
            @Header("orderId") final String orderId,
            @Payload final PlaceOrder placeOrder) throws Exception {
        orderService.placeOrder(orderId, placeOrder)
                .log("listener")
                .doFinally(v -> {
                    log.info("acknowledge message {}", placeOrder);
                    acknowledgment.acknowledge();
                }).subscribe();
    }


    @StreamEmitter
    @Output(MiddlewareChannels.ORDER_EVENTS)
    public Flux<Message<DomainEvent>> orderEvents(){
        return orderService.getEventStream()
                .map(t->createMessage(t.getT1(), t.getT2(), t.getT3()))
                .log("event-sender")
                .doOnNext(msg -> log.info("publishing message: {}", msg));
    }

    private Message<DomainEvent> createMessage(String orderId, Order.State state, DomainEvent event) {
        return MessageBuilder.withPayload(event)
                .setHeader("type", event.getType())
                .setHeader("orderId", orderId)
                .setHeader("state", state.toString())
                .build();
    }

}

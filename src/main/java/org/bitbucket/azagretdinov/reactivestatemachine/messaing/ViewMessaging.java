package org.bitbucket.azagretdinov.reactivestatemachine.messaing;


import org.bitbucket.azagretdinov.reactivestatemachine.ClientChannels;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderPlaced;
import org.bitbucket.azagretdinov.reactivestatemachine.event.OrderVerified;
import org.bitbucket.azagretdinov.reactivestatemachine.repository.OrderViewRepository;
import org.bitbucket.azagretdinov.reactivestatemachine.view.OrderView;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Currency;
import java.util.Objects;

@Component
@Profile("client")
public class ViewMessaging {

    private final OrderViewRepository orderViewRepository;

    public ViewMessaging(OrderViewRepository orderViewRepository) {
        this.orderViewRepository = orderViewRepository;
    }

    @StreamListener(target = ClientChannels.ORDER_EVENTS_VIEW, condition = "headers['type']=='order-placed'")
    public void viewUpdate(
            @Header("orderId") String orderId,
            @Header("state") String state,
            @Payload OrderPlaced event
    ){
        final var orderView = orderViewRepository.getOrderOrCreate(orderId);

        orderView.setOrderId(orderId);
        orderView.setState(state);

        orderView.setCurrency(format(event.getCurrency()));
        orderView.setCurrencyFractionDigits(event.getCurrency().getDefaultFractionDigits());
        orderView.setQuantity(event.getQuantity());
        orderView.setSymbol(event.getSymbol());
        orderView.setSide(Objects.toString(event.getSide()));

        orderView.setIssueDate(event.getIssueDate());
        orderView.setIssueTime(event.getIssueTime());

        orderView.setPlaceDate(event.getPlaceDate());
        orderView.setPlaceTime(event.getPlaceTime());
        orderView.setPlacedBy(event.getPlacedBy());

        orderViewRepository.save(orderView);
    }

    @StreamListener(target = ClientChannels.ORDER_EVENTS_VIEW, condition = "headers['type']=='order-verified'")
    public void viewUpdate(
            @Header("orderId") String orderId,
            @Header("state") String state,
            @Payload OrderVerified event
    ){
        final OrderView orderView = orderViewRepository.getOrderOrCreate(orderId);

        orderView.setOrderId(orderId);
        orderView.setState(state);
        orderView.setVerificationTime(event.getVerificationTime());

        orderViewRepository.save(orderView);
    }

    private String format(Currency currency) {
        return String.format("%s(%s)", currency.getCurrencyCode(), currency.getNumericCode());
    }
}
